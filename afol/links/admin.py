from django.contrib import admin

from afol.links.models import Link

admin.site.register(Link)
