from rest_framework import serializers

from afol.links.models import Link


class LinkSerializer(serializers.ModelSerializer):

    class Meta:
        fields = ('id', 'title', 'description', 'published',)
        model = Link
